<?php

namespace App\Service;

use App\Api\ApiResponse;
use App\Entity\Payment;
use App\Entity\Provider;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PaymentProvider
 * @package App\Service
 */
class PaymentProvider
{

    /**
     * @param string $url
     * @param string $token
     * @param array $data
     * @return array
     */
    private function postRequest($url, $token, $data)
    {
        //@todo need implementation to sent to provider by $url, $token, $data

        if (rand(1, 10) == 3) {
            return ['success' => false];
        }
        return ['success' => true];
    }

    /**
     * @param Provider $provider
     * @param $data
     * @return ApiResponse
     */
    public function sendTransfer(Provider $provider, $data)
    {
        $data = $this->postRequest(
            $provider->getApiUrl(), $provider->getApiToken(), $data
        );

        if ($data['success']) {
            $data['data']['details'] = 'Transfer was completed by ' . $provider->getName();
        } else {
            $data['data']['details'] = 'Transfer was rejected by ' . $provider->getName();
        }

        $response = new ApiResponse();
        $response->add($data, ['details']);

        return $response;
    }
}
