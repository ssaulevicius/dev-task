<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PaymentResponse
 * @package App\Service
 */
class PaymentResponse
{

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function data($data = [])
    {
        return new JsonResponse(['success' => true, 'data' => $data], Response::HTTP_OK);
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function created($data = [])
    {
        return new JsonResponse(['success' => true, 'data' => $data], Response::HTTP_CREATED);
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function updated($data = [])
    {
        return new JsonResponse(['success' => true, 'data' => $data], Response::HTTP_ACCEPTED);
    }

    /**
     * @param String $message
     *
     * @return JsonResponse
     */
    public function bad(String $message)
    {
        return new JsonResponse(['success' => false, 'message' => $message], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @return JsonResponse
     */
    public function badJsonFormat()
    {
        return $this->bad('Request must be in JSON format');
    }

    /**
     * @return JsonResponse
     */
    public function badAuthorization()
    {
        return $this->bad('Wrong authorization header');
    }

    /**
     * @return JsonResponse
     */
    public function badMissingField()
    {
        return $this->bad('Missing required field(s) or incorrect data');
    }

    /**
     * @return JsonResponse
     */
    public function badHourlyLimit()
    {
        return $this->bad('Your reached hourly limit for transactions');
    }

    /**
     * @return JsonResponse
     */
    public function badTransferLimit()
    {
        return $this->bad('Your all amounts (include this) reached transfer limit in this currency');
    }

    /**
     * @return JsonResponse
     */
    public function bad2FACode()
    {
        return $this->bad('Entered 2FA code is wrong');
    }

    /**
     * @return JsonResponse
     */
    public function badNotFoundByTransaction()
    {
        return $this->bad('There are no payments by this ID');
    }

    /**
     * @return JsonResponse
     */
    public function badSomethingWrong()
    {
        return $this->bad('Something went wrong, please contact to administrator');
    }
}
