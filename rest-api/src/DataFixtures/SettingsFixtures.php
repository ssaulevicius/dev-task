<?php

namespace App\DataFixtures;

use App\Entity\Settings;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class SettingsFixtures extends Fixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $settings = new Settings();
        $settings->setVersion(1);
        $settings->setApiToken('0a9df217ef6c409e13d7e20fd7642d66');
        $settings->setCode('111');
        $settings->setMaxAmountPerUser(1000);
        $settings->setMaxTransactionsPerHour(10);
        $settings->setDailyFeeRules(serialize(['100' => '10', '1000' => '5']));
        $manager->persist($settings);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
