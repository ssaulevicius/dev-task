<?php

namespace App\DataFixtures;

use App\Entity\Provider;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Currency;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use App\DataFixtures\ProviderFixtures;

/**
 * Class CurrencyFixtures
 * @package App\DataFixtures
 */
class CurrencyFixtures extends Fixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $currency = new Currency();
        $currency->setTitle('EUR');
        $currency->setEnabled(1);
        $currency->setProvider($this->getReference(ProviderFixtures::PROVIDER_MEGA_CASH));
        $manager->persist($currency);

        $currency = new Currency();
        $currency->setTitle('USD');
        $currency->setEnabled(1);
        $currency->setProvider($this->getReference(ProviderFixtures::PROVIDER_SUPER_MONEY));
        $manager->persist($currency);

        $currency = new Currency();
        $currency->setTitle('GBP');
        $currency->setEnabled(1);
        $currency->setProvider($this->getReference(ProviderFixtures::PROVIDER_SUPER_MONEY));
        $manager->persist($currency);

        $currency = new Currency();
        $currency->setTitle('RUB');
        $currency->setEnabled(1);
        $currency->setProvider($this->getReference(ProviderFixtures::PROVIDER_SUPER_MONEY));
        $manager->persist($currency);

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }

    public function getDependencies()
    {
        return array(
            ProviderFixtures::class
        );
    }
}
