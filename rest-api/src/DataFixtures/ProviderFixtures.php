<?php

namespace App\DataFixtures;

use App\Entity\Provider;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class ProviderFixtures extends Fixture implements OrderedFixtureInterface
{

    public const PROVIDER_MEGA_CASH = 'MEGA_CASH';
    public const PROVIDER_SUPER_MONEY = 'SUPER_MONEY';

    public function load(ObjectManager $manager)
    {
        $provider = new Provider();
        $provider->setName('Megacash');
        $provider->setApiUrl('http://megacash/api/payment/send');
        $provider->setApiToken('0a9df217ef6c409e13d7e20fd7642d66');
        $manager->persist($provider);
        $this->addReference(self::PROVIDER_MEGA_CASH, $provider);

        $provider = new Provider();
        $provider->setName('Supermoney');
        $provider->setApiUrl('http://supermoney/api/payment/send');
        $provider->setApiToken('2c64276d943707608b1dd2c9affbbd5c');
        $manager->persist($provider);
        $this->addReference(self::PROVIDER_SUPER_MONEY, $provider);

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
