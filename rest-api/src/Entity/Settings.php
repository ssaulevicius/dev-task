<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SettingsRepository")
 * @ORM\Table(name="settings")
 */
class Settings
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $apiToken;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $code;

    /**
     * @ORM\Column(type="float", options={"default": "0"}, nullable=true)
     */
    private $maxAmountPerUser;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxTransactionsPerHour;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $dailyFeeRules;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getVersion(): ?int
    {
        return $this->version;
    }

    /**
     * @param int $version
     * @return Settings
     */
    public function setVersion(int $version): self
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getApiToken(): ?string
    {
        return $this->apiToken;
    }

    /**
     * @param string|null $apiToken
     * @return Provider
     */
    public function setApiToken(string $apiToken): self
    {
        $this->apiToken = $apiToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Provider
     */
    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return float
     */
    public function getMaxAmountPerUser(): ?float
    {
        return $this->maxAmountPerUser;
    }

    /**
     * @param float $maxAmountPerUser
     * @return Provider
     */
    public function setMaxAmountPerUser(float $maxAmountPerUser): self
    {
        $this->maxAmountPerUser = $maxAmountPerUser;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxTransactionsPerHour(): ?int
    {
        return $this->maxTransactionsPerHour;
    }

    /**
     * @param int $maxTransactionsPerHour
     * @return Provider
     */
    public function setMaxTransactionsPerHour(int $maxTransactionsPerHour): self
    {
        $this->maxTransactionsPerHour = $maxTransactionsPerHour;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDailyFeeRules(): ?string
    {
        return $this->dailyFeeRules;
    }

    /**
     * @param string|null $dailyFeeRules
     * @return Provider
     */
    public function setDailyFeeRules(string $dailyFeeRules): self
    {
        $this->dailyFeeRules = $dailyFeeRules;

        return $this;
    }
}
