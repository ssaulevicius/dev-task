<?php

namespace App\Repository;

use App\Entity\Currency;
use App\Entity\Payment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Payment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Payment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Payment[]    findAll()
 * @method Payment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Payment::class);
    }

    /**
     * @param int $userId
     * @param Currency $curr
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getTotalAmountPerUser(int $userId, Currency $curr)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('(SUM(p.amount)+SUM(p.fee)) AS total')
            ->andWhere('p.userId = :userId')
            ->setParameter('userId', $userId)
            ->andWhere('p.currency = :currency')
            ->setParameter('currency', $curr);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param int $userId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCurrentDayAmountPerUser(int $userId)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('SUM(p.amount) AS total')
            ->andWhere('p.userId = :userId')
            ->setParameter('userId', $userId)
            ->andWhere('p.createdDate >= :today')
            ->setParameter('today', date('Y-m-d', time()));

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param int $userId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getHourlyCountPerUser(int $userId)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('count(p.userId) AS count')
            ->andWhere('p.userId = :userId')
            ->setParameter('userId', $userId)
            ->andWhere('p.createdDate >= :today')
            ->setParameter('today', date('Y-m-d H:i:s', time() - 3600));

        return $qb->getQuery()->getSingleScalarResult();
    }
}
