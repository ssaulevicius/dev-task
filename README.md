### KERNOLAB DEV TASK

```
kernolab_task.pdf
```

#### Start the project

Download or git clone the project
```
git clone git@bitbucket.org:ssaulevicius/kernolab-dev-task.git
or
git clone https://ssaulevicius@bitbucket.org/ssaulevicius/kernolab-dev-task.git
```

Run project with Docker
```
docker-compose up
```

Initialize project structure, composer, create DB structures, load fixtures, ...
```
$ sh init.sh
```

##### NOTE

If you have network conflicts to run docker there are two solutions to solve the problem:

Solution
```
$ docker network prune
``` 

Solution
```
Change IP address 172.18.0.0/24 (172.18.0.2-172.18.0.1) on docker-compose.yml
``` 

#### Accessing the project

Front-end site
```
http://localhost:8082/
user1: user1@domain.com:passowrd1
user2: user2@domain.com:password2
```

Get containers IP addresses
```
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' sf_apache sf_php sf_mysql sf_devops sf_phpmyadmin
```

Run REST API tests
```
docker exec -it sf_php /bin/bash -c 'cd rest-api; ./vendor/bin/codecept run api'
```

REST API host you can get IP by "sf_devops" container (default 172.18.0.6)
```
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' sf_devops
```

REST API methods
```
GET     /api/payment/{transactionId}
POST    /api/payment
PATCH   /api/payment/confirm
```

Background payments update tasks (with CRON in sf_devops container)
```
* * * * * root php /home/www/rest-api/bin/console provider:send
* * * * * root php /home/www/front-end/bin/console payments:update
```

Run manually REST API background payment transfers to provider
```
docker exec -it sf_php /bin/bash -c 'cd rest-api; php bin/console provider:send'
```

Run Front-end background payment status updated
```
docker exec -it sf_php /bin/bash -c 'cd front-end; php bin/console payments:update'
```

Accessing docker container (sf_apache sf_php sf_mysql sf_devops sf_phpmyadmin)
```
docker exec -it {container_name} /bin/bash
```

PhpMyAdmin host (default 172.18.0.3)
```
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' sf_phpmyadmin
```

