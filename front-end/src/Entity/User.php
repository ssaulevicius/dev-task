<?php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Balance", mappedBy="user")
     */
    protected $balances;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Payment", mappedBy="user")
     */
    protected $payments;

    /**
     * Get balances
     *
     * @return ArrayCollection
     */
    public function getBalances()
    {
        return $this->balances;
    }

    /**
     * Get payments
     *
     * @return ArrayCollection
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @param Currency $currency
     * @return Balance|null
     */
    public function getBalanceByCurrency(Currency $currency)
    {
        /** @var Balance $balance */
        foreach ($this->balances as $balance) {
            if ($balance->isCurrency($currency->getTitle())) {
                return $balance;
            }
        }

        return null;
    }
}
