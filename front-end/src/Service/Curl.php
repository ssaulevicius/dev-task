<?php

namespace App\Service;

class Curl
{

    /**
     * @param string $url
     * @param string $authorization
     * @return mixed
     */
    public function requestGet($url, $authorization)
    {

        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => $authorization,
        ];

        $client = new \GuzzleHttp\Client([
            'headers' => $headers
        ]);

        $response = $client->get($url);

        return $response->getBody()->getContents();
    }

    /**
     * @param string $url
     * @param array $data
     * @param string $authorization
     * @return mixed
     */
    public function requestPost($url, $data, $authorization)
    {
        $headers = [
            'Authorization' => $authorization,
            'Content-Type' => 'application/json'
        ];

        $client = new \GuzzleHttp\Client([
            'headers' => $headers
        ]);

        $response = $client->post($url, [
            \GuzzleHttp\RequestOptions::JSON => $data
        ]);

        return $response->getBody()->getContents();
    }

    /**
     * @param string $url
     * @param array $data
     * @param string $authorization
     * @return mixed
     */
    public function requestPatch($url, $data, $authorization)
    {
        $headers = [
            'Authorization' => $authorization,
            'Content-Type' => 'application/json'
        ];

        $client = new \GuzzleHttp\Client([
            'headers' => $headers
        ]);

        $response = $client->patch($url, [
            \GuzzleHttp\RequestOptions::JSON => $data
        ]);

        return $response->getBody()->getContents();
    }
}
