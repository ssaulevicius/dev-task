<?php

namespace App\Command;

use App\Entity\Payment;
use App\Service\PaymentApi;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class Update
 * @package App\Command
 */
class Update extends Command
{

    protected function configure()
    {
        $this
            ->setName('payments:update')
            ->setDescription('Update payments statuses from payment gateway service');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->newLine();
        $io->title('Starting update process');

        /** @var PaymentApi $paymentApi */
        $paymentApi = $this->getApplication()->getKernel()->getContainer()->get('paymentApi');

        /** @var EntityManagerInterface $em */
        $em = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getEntityManager();

        $confirmedPayments = $em->getRepository(Payment::class)->findBy(['status' => Payment::STATUS_CONFIRMED]);

        /** @var Payment $payment */
        foreach ($confirmedPayments as $payment) {

            $io->section('TRANSACTION ID ' . $payment->getTransactionId());

            $response = $paymentApi->getPaymentInfo($payment->getTransactionId());

            if ($response->isSuccess()) {
                switch ($response->getData('status')) {
                    case Payment::GATEWAY_STATUS_COMPLETED:
                        $payment->setStatus(Payment::STATUS_COMPLETED);
                        $io->success('--- COMPLETED ---');
                        break;
                    case Payment::GATEWAY_STATUS_REJECTED:
                        $payment->setStatus(Payment::GATEWAY_STATUS_REJECTED);
                        $payment->getUser()->getBalanceByCurrency(
                            $payment->getCurrency()
                        )->upBalance(
                            $payment->getAmount() + $payment->getFee()
                        );
                        $io->comment('--- REJECTED ---');
                        break;
                    case Payment::GATEWAY_STATUS_CONFIRMED:
                        $io->comment('--- CONFIRMED (STATUS NOT CHANGED) ---');
                        break;
                }
                $io->listing($response->getAllData());

                $payment->setDetails($response->getData('details'));
                $payment->setUpdatedDate(new \DateTime());
                $em->flush();
            } else {
                $io->error('unsuccessfully: ' . $response->getMessage());
            }
        }
    }
}
