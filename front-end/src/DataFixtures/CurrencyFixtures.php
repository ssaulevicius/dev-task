<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Currency;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

/**
 * Class CurrencyFixtures
 * @package App\DataFixtures
 */
class CurrencyFixtures extends Fixture implements OrderedFixtureInterface
{
    public const CURRENCY_EUR = 'EUR';
    public const CURRENCY_USD = 'USD';
    public const CURRENCY_GBP = 'GBP';
    public const CURRENCY_RUB = 'RUB';

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $currency = new Currency();
        $currency->setTitle('EUR');
        $currency->setEnabled(1);
        $manager->persist($currency);
        $this->addReference(self::CURRENCY_EUR, $currency);

        $currency = new Currency();
        $currency->setTitle('USD');
        $currency->setEnabled(1);
        $manager->persist($currency);
        $this->addReference(self::CURRENCY_USD, $currency);

        $currency = new Currency();
        $currency->setTitle('GBP');
        $currency->setEnabled(1);
        $manager->persist($currency);
        $this->addReference(self::CURRENCY_GBP, $currency);

        $currency = new Currency();
        $currency->setTitle('RUB');
        $currency->setEnabled(1);
        $manager->persist($currency);
        $this->addReference(self::CURRENCY_RUB, $currency);

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
