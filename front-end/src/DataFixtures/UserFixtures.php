<?php

namespace App\DataFixtures;

use App\Entity\Balance;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserManagerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class UserFixtures extends Fixture implements OrderedFixtureInterface
{

    public const USER1 = 'USER1';
    public const USER2 = 'USER2';

    private $userManager;

    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    public function load(ObjectManager $manager)
    {
        $user1 = $this->userManager->createUser();
        $user1->setUsername('user1');
        $user1->setEmail('user1@domain.com');
        $user1->setPlainPassword('password1');
        $user1->setEnabled(true);
        $user1->setRoles(array('ROLE_USER'));
        $this->userManager->updateUser($user1);
        $this->addReference(self::USER1, $user1);

        $user2 = $this->userManager->createUser();
        $user2->setUsername('user2');
        $user2->setEmail('user2@domain.com');
        $user2->setPlainPassword('password2');
        $user2->setEnabled(true);
        $user2->setRoles(array('ROLE_USER'));
        $this->userManager->updateUser($user2);
        $this->addReference(self::USER2, $user2);
    }

    public function getOrder()
    {
        return 1;
    }
}
