<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Class MainController
 * @package App\Controller
 */
class MainController extends Controller
{

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function homeAction(Request $request)
    {
        return $this->render('main/home.html.twig');
    }

}

